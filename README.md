# Fitpeo practical app

## Write a android machine test with calling get API and populated all the data in recycler view, on tap detail activity open with header image, title and description .

### Get API: https://jsonplaceholder.typicode.com/photos

- MVVM design pattern
- Retrofit Client
- Dagger 2 or similar library
- Kotlin
- Data binding and View binding
- Use Picasso library for image rendering
- Write unit test cases
