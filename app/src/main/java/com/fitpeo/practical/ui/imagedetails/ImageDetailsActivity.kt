package com.fitpeo.practical.ui.imagedetails

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.viewpager.widget.ViewPager
import com.fitpeo.practical.R
import com.fitpeo.practical.adapter.ImageShowAdapter
import com.fitpeo.practical.data.remote.ImageData
import com.fitpeo.practical.databinding.ActivityImageDetailsBinding
import com.fitpeo.practical.databinding.DialogDetailsBinding
import com.fitpeo.practical.utils.ZoomOutTransformation
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squareup.picasso.Picasso
import java.text.FieldPosition

class ImageDetailsActivity : AppCompatActivity() {

    lateinit var binding: ActivityImageDetailsBinding

    companion object {
        lateinit var imageList: ArrayList<ImageData>
        var position: Int = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImageDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initImageAdapter()
    }

    private fun initImageAdapter() {
        val zoomOutTransformation = ZoomOutTransformation()
        binding.viewPager.setPageTransformer(true, zoomOutTransformation)
        val mImageShowAdapter = ImageShowAdapter(this, imageList)

        binding.viewPager.adapter = mImageShowAdapter
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
        })
        binding.viewPager.setCurrentItem(position, false)
        binding.imgBack.setOnClickListener {
            finish()
        }

        binding.imgDetails.setOnClickListener {
            showDetailsBottomSheet()
        }
    }

    private fun showDetailsBottomSheet(){
        val dialog = BottomSheetDialog(this, R.style.DialogTheme)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val mDialogDetailsBinding = DialogDetailsBinding.inflate(LayoutInflater.from(this))
        dialog.setContentView(mDialogDetailsBinding.root)
        val imageData =  imageList[binding.viewPager.currentItem]
        Picasso.get().load(imageData.url).placeholder(R.color.grey__).into(mDialogDetailsBinding.ivCoverImage)
        mDialogDetailsBinding.txtTitle.text = imageData.title
        dialog.show()
    }
}