package com.fitpeo.practical.ui.home

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import com.fitpeo.practical.R
import com.fitpeo.practical.adapter.ViewPagerAdapter
import com.fitpeo.practical.data.remote.ImageData
import com.fitpeo.practical.databinding.FragmentHomeBinding
import com.fitpeo.practical.rest.Resource
import com.fitpeo.practical.utils.Constant
import com.fitpeo.practical.utils.Constant.DELAY
import com.fitpeo.practical.utils.Constant.SMOOTH_DELAY
import com.fitpeo.practical.utils.InternetConnectionCheck
import com.fitpeo.practical.utils.Utils
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.fragment_home) {

    var binding: FragmentHomeBinding? = null
    private val viewModel: HomeViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHomeBinding.bind(view)
        Handler(Looper.myLooper()!!).postDelayed({ init() },SMOOTH_DELAY)
    }

    private fun callPhotosAPI(isConnect: Boolean) {
        if (isConnect) {

            if (viewModel.mutablePhotosList.value == null) {
                binding?.isLoading = true
                viewModel.getPhotos()
            }
            isConnectivityStatus(true)
        } else {
            isConnectivityStatus(false)
        }
    }

    private fun connectivityObserver() {
        InternetConnectionCheck(requireContext()).observe(viewLifecycleOwner) { isConnected ->
            callPhotosAPI(isConnected)
        }
    }

    private fun init() {
        connectivityObserver()
        callPhotosAPI(Utils.isInternetAvailable(requireContext()))
        viewModel.mutablePhotosList.observe(viewLifecycleOwner) {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding?.isLoading = false
                    val map = viewModel.setData(it.data)
                    setAdapter(map)
                }
                Resource.Status.ERROR -> {
                    binding?.isLoading = false
                }
            }
        }
    }

    private fun setAdapter(imageMap: HashMap<Int, ArrayList<ImageData>>) {
        setupViewPager(imageMap)
        setupTabLayout()
    }

    private fun setupTabLayout() {
        TabLayoutMediator(
            binding!!.tabLayout, binding!!.pager
        ) { tab, position -> tab.text = "Album " + (position + 1) }.attach()
    }

    private fun setupViewPager(imageMap: HashMap<Int, ArrayList<ImageData>>) {
        val adapter = ViewPagerAdapter(requireActivity(), imageMap)
        binding?.pager?.adapter = adapter
    }

    private fun isConnectivityStatus(isConnected: Boolean) {
        if (!isConnected) {
            binding?.isConnected = false
            setConnectivityStatusData(getString(R.string.no_internet_connection), R.color.white, R.color.theme_color)
        } else if (binding!!.isConnected == false) {
            setConnectivityStatusData(getString(R.string.back_online), R.color.white, R.color.green)
            Handler(Looper.myLooper()!!).postDelayed({ binding?.isConnected = true }, DELAY)
        }else{
            binding?.isConnected = true  // First time network is available
        }
    }

    private fun setConnectivityStatusData(message: String?, textColor: Int, bgColor: Int) {
        binding?.txtError?.text = message
        binding?.txtError?.setTextColor(ContextCompat.getColor(requireContext(), textColor))
        binding?.txtError?.setBackgroundColor(ContextCompat.getColor(requireContext(), bgColor))
    }

}
