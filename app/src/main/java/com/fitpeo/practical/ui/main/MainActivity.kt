package com.fitpeo.practical.ui.main

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.PopupMenu
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.fitpeo.practical.R
import com.fitpeo.practical.databinding.ActivityMainBinding
import com.fitpeo.practical.rest.Resource
import com.fitpeo.practical.ui.home.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private var keep = true
    private val delay = 2000L
    lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // For splash screen Delay
        splashScreen.setKeepOnScreenCondition { keep }
        val handler = Handler(Looper.myLooper()!!)
        handler.postDelayed({
            keep = false
            init()
        }, delay)

    }

    private fun init() {
        navController = findNavController(R.id.mainFragment) //setup bottom navigation
        setupSmoothBottomMenu()
    }

    private fun setupSmoothBottomMenu() {
        val popupMenu = PopupMenu(this, null)
        popupMenu.inflate(R.menu.bottom_menu)
        val menu = popupMenu.menu
        binding.bottomBar.setupWithNavController(menu, navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }




}