package com.fitpeo.practical.ui.download

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.fitpeo.practical.R
import com.fitpeo.practical.databinding.FragmentDownloadBinding
import com.fitpeo.practical.databinding.FragmentFavoriteBinding
import com.fitpeo.practical.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DownloadFragment : Fragment(R.layout.fragment_download) {

    private var binding: FragmentDownloadBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDownloadBinding.bind(view)

    }

}
