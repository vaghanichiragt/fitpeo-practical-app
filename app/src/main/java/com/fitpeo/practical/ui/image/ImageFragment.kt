package com.fitpeo.practical.ui.image

import android.R.attr.spacing
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.fitpeo.practical.R
import com.fitpeo.practical.adapter.ImageAdapter
import com.fitpeo.practical.data.remote.ImageData
import com.fitpeo.practical.databinding.FragmentImageBinding
import com.fitpeo.practical.utils.GridSpacingItemDecoration
import com.fitpeo.practical.utils.toPx
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ImageFragment : Fragment(R.layout.fragment_image) {

    lateinit var binding: FragmentImageBinding
    var list : ArrayList<ImageData>? =null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentImageBinding.bind(view)
        setAdapter()
    }

    private fun setAdapter(){
        val manager = GridLayoutManager(requireActivity(),2)
        binding.rvPhotos.layoutManager = manager
        binding.rvPhotos.addItemDecoration(GridSpacingItemDecoration(2, 12f.toPx.toInt(), true))
        binding.rvPhotos.adapter = list?.let { ImageAdapter(requireActivity(), it) }
    }

}
