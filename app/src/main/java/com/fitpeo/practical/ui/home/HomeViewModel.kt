package com.fitpeo.practical.ui.home

import android.media.Image
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fitpeo.practical.data.remote.ImageData
import com.fitpeo.practical.rest.ApiRepository
import com.fitpeo.practical.rest.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: ApiRepository):ViewModel() {

    var mutablePhotosList = MutableLiveData<Resource<List<ImageData>>>()

    fun getPhotos()  = viewModelScope.launch {
        repository.getPhotos().let {
            Log.d("TAG", "getPhotos: " + "12433")
            if (it.isSuccessful){
                mutablePhotosList.postValue(Resource.success(it.body()))
            }else{
                mutablePhotosList.postValue(Resource.error(it.errorBody().toString(), null))
            }
        }
    }

    fun setData(data: List<ImageData>?): HashMap<Int, ArrayList<ImageData>> {
        val hashMap : HashMap<Int,ArrayList<ImageData>> = HashMap()
        if (data.isNullOrEmpty().not()){
            data?.forEachIndexed { _, imageData ->
                if (hashMap.containsKey(imageData.albumId)){
                    val list = hashMap.getValue(imageData.albumId!!)
                    list.add(imageData)
                    hashMap[imageData.albumId] = list
                }else{
                    val imageList = ArrayList<ImageData>()
                    imageList.add(imageData)
                    hashMap[imageData.albumId!!] = imageList
                }
            }
        }
        return hashMap
    }

}
