package com.fitpeo.practical.ui.favorite

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.fitpeo.practical.R
import com.fitpeo.practical.databinding.FragmentFavoriteBinding
import com.fitpeo.practical.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoriteFragment : Fragment(R.layout.fragment_favorite) {

    private var binding: FragmentFavoriteBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentFavoriteBinding.bind(view)

    }

}
