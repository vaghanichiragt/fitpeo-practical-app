package com.fitpeo.practical.utils

object Constant {

    const val API_BASE_URL = "https://jsonplaceholder.typicode.com/"
    const val IMAGE_LIST = "IMAGE_LIST"
    const val DELAY = 3000L
    const val SMOOTH_DELAY = 300L


}