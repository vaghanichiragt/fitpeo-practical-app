package com.fitpeo.practical.utils

import android.content.res.Resources
import android.util.TypedValue
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.fitpeo.practical.R
import com.squareup.picasso.Picasso

@BindingAdapter("url")
fun bindUrlImage(view: ImageView, imageUrl: String?) {
    if (imageUrl != null) {
        Picasso.get()
            .load(imageUrl)
            .placeholder(R.color.grey__)
            .fit()
            .into(view)
    } else {
        view.setImageBitmap(null)
    }
}

val Float.toPx get() = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP,
    this.toFloat(),
    Resources.getSystem().displayMetrics)

