package com.fitpeo.practical.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.fitpeo.practical.R
import com.fitpeo.practical.data.remote.ImageData
import com.fitpeo.practical.databinding.ItemImagesBinding
import com.fitpeo.practical.ui.imagedetails.ImageDetailsActivity
import com.squareup.picasso.Picasso

class ImageAdapter(val context: Context, val imageList: ArrayList<ImageData>) :
    RecyclerView.Adapter<ImageAdapter.ImageHolder>() {


    class ImageHolder(internal val binding: ItemImagesBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val binding: ItemImagesBinding = ItemImagesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ImageHolder(binding)
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        Picasso.get().load(imageList[position].url).placeholder(R.color.grey__).into(holder.binding.ivCoverImage)
        holder.binding.root.setOnClickListener {
            ImageDetailsActivity.imageList = imageList
            ImageDetailsActivity.position = position
            val intent = Intent(context, ImageDetailsActivity::class.java)
            context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return imageList.size
    }
}