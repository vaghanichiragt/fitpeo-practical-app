package com.fitpeo.practical.adapter

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.fitpeo.practical.data.remote.ImageData
import com.fitpeo.practical.ui.image.ImageFragment
import com.fitpeo.practical.utils.Constant
import com.google.gson.Gson


class ViewPagerAdapter(fragmentActivity: FragmentActivity, private val imageList : HashMap<Int,ArrayList<ImageData>>) :
    FragmentStateAdapter(fragmentActivity) {
 
    override fun getItemCount(): Int {
        return imageList.size
    }

    override fun createFragment(position: Int): ImageFragment {
        val imageFragment = ImageFragment()
        imageFragment.list = imageList[imageList.keys.toList()[position]]
        return imageFragment
    }
}
