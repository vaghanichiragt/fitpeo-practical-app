package com.fitpeo.practical.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.fitpeo.practical.data.remote.ImageData
import com.fitpeo.practical.databinding.ItemFullImageBinding

class ImageShowAdapter(val context:Context, private val imageList: ArrayList<ImageData>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = ItemFullImageBinding.inflate(layoutInflater)
        binding.imageData = imageList[position]
        binding.executePendingBindings()
        container.addView(binding.root)
        return binding.root
    }

    override fun getCount(): Int = imageList.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}
