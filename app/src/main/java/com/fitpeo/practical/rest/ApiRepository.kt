package com.fitpeo.practical.rest

import javax.inject.Inject

class ApiRepository

@Inject  constructor(val apiService: ApiService){
    suspend fun getPhotos() = apiService.getPhotos()
}