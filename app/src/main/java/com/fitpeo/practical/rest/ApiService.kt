package com.fitpeo.practical.rest

import com.fitpeo.practical.data.remote.ImageData
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("photos")
    suspend fun getPhotos(): Response<List<ImageData>>
}