package com.fitpeo.practical.data.local

data class NetworkStatus(
    val bgColor: Int? = null,
    val textColor: Int? = null,
    val text: String? = null,
)