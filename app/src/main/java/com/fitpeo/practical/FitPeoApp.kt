package com.fitpeo.practical

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FitPeoApp : Application()